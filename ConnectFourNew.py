# Graphic-based Connect Four Game (Updated 10.11.2022)
# By: William (Trey) Phillips

# This is a recent rebuild of ConnectFour.py that better illustrates my current capabilities

from graphics import *
import random


def draw_circle(window, r, anchor, color):
    """
    Draws a circle object with the given radius and color on the given window centered on the anchor
    @param window: Graphics.GraphWin -> Window to draw circle to
    @param r: Number -> Radius of circle
    @param anchor: Graphics.Point -> Location where circle will be centered
    @param color: String -> Color recognized by tkinter
    @return: Graphics.Circle -> Graphics.Circle object with given properties
    """

    circ = Circle(anchor, r)
    circ.setFill(color)
    circ.draw(window)
    return circ


def draw_rectangle(window, p1, p2, color):
    """
    Draw's a rectangle of the given color on the given window with one corner at p1 and
    an opposing corner at p2
    @param window: Graphics.GraphWin -> Window to draw rectangle to
    @param p1: Graphics.Point -> Location of a corner of the rectangle
    @param p2: Graphics.Point -> Location of the rectangles corner opposite p1
    @param color: String -> Color recognized by tkinter
    @return: Graphics.Rectangle -> Graphics.Rectangle object with given properties
    """

    rect = Rectangle(p1, p2)
    rect.setFill(color)
    rect.draw(window)
    return rect


def draw_text(window, anchor, text, size, color):
    """
    Draws a string of text to a given window with a given size and color, centered at anchor
    @param window: Graphics.GraphWin -> Window to draw text on
    @param anchor: Graphics.Point -> Location where text will be centered
    @param text: String -> Word(s) that text will contain
    @param color: String -> Color recognized by tkinter
    @return: Graphics.Text -> Graphics.Text object with given properties
    """

    txt = Text(anchor, text)
    txt.setSize(size)
    txt.setTextColor(color)
    txt.draw(window)
    return txt


class ScoreBoard:
    # Class representing a scoreboard comparing two parties

    def __init__(self, player1_color, player2_color):
        """
        Initializes a ScoreBoard object
        @param player1_color: String -> Color recognized by tkinter
        @param player2_color: String -> Color recognized by tkinter
        """

        # Initial win count for player1 and player2 
        self.wins0, self.wins1 = 0, 0

        self.player1_color = player1_color
        self.player2_color = player2_color
        
        # String -> String representing current scores
        self.str = f"{self.player1_color} Wins: {self.wins0}\t{self.player2_color} Wins: {self.wins1}"
        
        # Null Graphics.Text and Graphics.GraphWin objects
        self.txt, self.window = None, None


    def draw(self, window, anchor, size):
        """
        Draws this ScoreBoard to the given window at the given size, centered upon anchor
        @param window: Graphics.GraphWin -> Window to draw scoreboard on
        @param anchor: Graphics.Point -> Location where this ScoreBoard will be centered
        @param size: Int -> TextSize recognized by tkinter
        """

        # Sets self.txt and self.window to non-null Graphics.Text and Graphics.GraphWin objects
        self.txt, self.window = draw_text(window, anchor, self.str, size, "White"), window

    def undraw(self):
        """
        Removes this ScoreBoard from any window it may have been drawn to
        """

        # Checks if self.txt is Null to know if ScoreBoard has been drawn yet
        if self.txt != None:
            self.txt.undraw()


    def update(self, player):
        """
        Increments Win Count for the given player and updates text of this ScoreBoard's txt property
        @param player: Int -> Number corresponding to player who's score is to incremented
        """

        if player:
            self.wins1 += 1
        else:
            self.wins0 += 1
        
        self.undraw()
        self.str = f"{self.player1_color} Wins: {self.wins0}\t{self.player2_color} Wins: {self.wins1}"
        self.txt.setText(self.str)
        self.txt.draw(self.window)


class ColorPicker:
    # An object that allows users to select colors from a color palette

    def __init__(self, window_name, width, height):
        """
        Initializes ColorPicker
        @param window_name: String -> Name of this ColorPicker's window
        @param width: Number -> Width of this ColorPicker's window
        @param height: Number -> Height of this ColorPicker's window
        """

        # Initializes Graphics.GraphWin object that will act as a color palette
        self.window = GraphWin(window_name, width, height)

        # String[][] -> Arrangement of colors that may be chosen from
        color_names = [
            ["Red", "Orange", "Yellow", "Green"],
            ["Cyan", "Blue", "Purple", "Magenta"]
        ]

        # Dict -> Relates colors to the coordinates they will have on the color palette
        self.color_coord_dict = {color: (x, y) for y, row in enumerate(color_names) for x, color in enumerate(row)}
        
        # Sets coordinates of self.window to fit color_names
        self.window.setCoords(0, len(color_names), len(color_names[0]), 0)

        # Draws a rectangle for each color to self.window
        for color_name in self.color_coord_dict.keys():
            color_x, color_y = self.color_coord_dict[color_name]
            draw_rectangle(self.window, Point(color_x, color_y), Point(color_x + 1, color_y + 1), color_name)

        
    def get_color(self):
        """
        Waits for user to click and then determines which color the user clicked on
        @return: String -> Selected color's name
        """

        # Receives mouse input from user and unpacks it into its x and y values
        click = self.window.getMouse()
        click_x, click_y = click.getX(), click.getY()

        # Checks each color to see if the input falls within it's rectangle
        for color_name in self.color_coord_dict.keys():
            color_x, color_y = self.color_coord_dict[color_name]
            if color_x < click_x < color_x + 1 and color_y < click_y < color_y + 1:
                self.window.close()
                return color_name


class Button:
    # Class representing a graphic button

    def __init__(self, text, anchor, height, width, color):
        """
        Initializes a Button on the given window with a given label at a given anchor of a given
        height, width, and color
        @param text: String -> Text to be written
        @param anchor: Graphics.Point -> Location where Button will be centered
        @param height: Number -> Height of the Button
        @param width: Number -> Width of the Button
        @param color: String -> Color recognized by tkinter
        """

        # Derives xUL, yUL, xLR, and yLR from provided anchor, height, and width
        self.xUL = anchor.getX() - 0.5 * width
        self.yUL = anchor.getY() + 0.5 * height
        self.xLR = anchor.getX() + 0.5 * width
        self.yLR = anchor.getY() - 0.5 * height

        # Creates a Graphics.Text object at anchor of the given text and sets it's size
        self.text = Text(anchor, text)
        self.text.setSize(20)
        
        # Creates a rectangle and sets it's color
        self.shape = Rectangle(Point(self.xUL, self.yUL), Point(self.xLR, self.yLR))
        self.shape.setFill(color)
        

    def is_in(self, point):
        """
        Determines if the given point falls within this Button
        @param point: Graphics Point Object
        @return: Boolean -> Point is within this Button
        """

        return self.xUL < point.getX() < self.xLR and self.yLR < point.getY() < self.yUL


    def draw(self, window):
        """
        Draws this Button on the given window
        @param window: Graphics.GraphWin -> Window to be drawn to
        """

        self.shape.draw(window)
        self.text.draw(window)


    def undraw(self):
        """
        Undraws this Button from any window it may be drawn to
        """

        self.shape.undraw()
        self.text.undraw()


class Token:
    # Class representing a game piece

    def __init__(self, r, x, y, color):
        """
        Initializes a Token at (x, y) with the given radius and color
        @param r: Number -> Radius of this Token
        @param x: Number -> x-coordinate of this Token
        @param y: Number -> y-coordinate of this Token
        @param color: String -> Color recognized by tkinter
        """
        
        # Sets initial values of self.x, self.y, and self.color
        self.x, self.y, self.color = x, y, color

        # Creates two shades of self.color
        self.color2, self.color4 = f"{self.color}2", f"{self.color}4"

        # Creates an edge for this Token
        self.edge = Circle(Point(self.x, self.y), r)
        self.edge.setFill(self.color4)

        # Creates the middle portion of this Token
        self.inner = Circle(Point(self.x, self.y), r - (r/5))
        self.inner.setFill(self.color2)

        # Creates a logo that is printed to the middle of this Token
        self.logo = Text(Point(self.x, self.y), chr(960))
        self.logo.setSize(36)
        self.logo.setTextColor(self.color4)
        self.logo.setStyle("bold")


    def draw(self, window):
        """
        Draws this Token to a given window
        @param window: Graphics.GraphWin -> Window to be drawn on
        """

        self.edge.draw(window)
        self.inner.draw(window)
        self.logo.draw(window)


    def undraw(self):
        """
        Undraws this Token from any windows it may have been drawn to
        """

        self.logo.undraw()
        self.inner.undraw()
        self.edge.undraw()
        

class ConnectFour:
    # Class representing a game of Connect Four

    def __init__(self):
        """
        Initializes a game of ConnectFour
        """
        
        # Creates the underlying data structure of this ConnectFour game
        self.board = [
            [None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None],
            [None, None, None, None, None, None, None]
        ]

        # Creates the Graphic that this ConnectFour game will be played upon
        self.scalar = 150
        self.window_width = self.scalar * 7
        self.window_height = int(self.scalar * 6)
        self.window = GraphWin("Connect Four", self.window_width, self.window_height + 30)
        self.board_color = "Black"
        self.tokens_on_board = []
        self.column_width = self.window_width // 7
        self.row_width = self.window_height // 6
        self.col_barriers = [i for i in range(0, self.window_width + 1, self.column_width)]
        self.row_barriers = [i for i in range(0, self.window_height + 1, self.row_width)]
        self.col_middle_list = [((self.col_barriers[i] + self.col_barriers[i + 1]) / 2) for i in range(7)]
        self.row_middle_list = [((self.row_barriers[i] + self.row_barriers[i + 1]) / 2) for i in range(6)]
        self.spot_points = [(x, y) for x in self.col_middle_list for y in self.row_middle_list]
        self.window.setBackground(self.board_color)

        # Initializes players and Null properties for player_colors, scoreboard, and win_back
        self.players = [0, 1]
        self.player_colors = [None, None]
        self.scoreboard = None
        self.win_back = None


    def build_board(self):
        """
        Draws white circles upon this ConnectFour game's window that represent spots where pieces can be placed
        """
        
        # derives spot_radius from window size and draws circles of that size
        spot_radius = (self.column_width / 2) - (self.column_width / 14)
        for spot in self.spot_points:
            draw_circle(self.window, spot_radius, Point(spot[0], spot[1]), "White")



    def check_horizontal(self):
        """
        Checks if there is a horizontal line of four like pieces present on the board
        @return: Bool -> Presence of a horizontal line of four similar pieces
        """

        for i in range(6):
            curr_row = self.board[i]
            for j in range(4):
                curr_four = curr_row[j: j + 4]
                if curr_four.count(curr_four[0]) == 4 and curr_four[0] != None:
                    return True
        return False

    
    def check_vertical(self):
        """
        Checks if there is a vertical line of four like pieces present on the board
        @return: Bool -> Presence of a vertical line of four similar pieces
        """
        
        for j in range(7):
            curr_col = [self.board[i][j] for i in range(6)]
            for i in range(4):
                curr_four = curr_col[i: i + 4]
                if curr_four.count(curr_four[0]) == 4 and curr_four[0] != None:
                    return True
        return False


    def check_diagonal(self):
        """
        Checks if there is a diagonal line of four like pieces present on the board
        @return: Bool -> Presence of a diagonal line of four similar pieces
        """

        for i in range(3):
            for j in range(4):
                curr_diag_ULLR = [self.board[i + k][j + k] for k in range(4)]
                curr_diag_LLUR = [self.board[i + k][j + 3 - k] for k in range(4)]
                if curr_diag_ULLR.count(curr_diag_ULLR[0]) == 4 and curr_diag_ULLR[0] != None or curr_diag_LLUR.count(curr_diag_LLUR[0]) == 4 and curr_diag_LLUR[0] != None:
                    return True
        return False
          
    
    def check_for_winner(self):
        """
        Checks all possible win conditions
        @return: Bool -> Win condition has been met
        """

        return True if self.check_horizontal() or self.check_vertical() or self.check_diagonal() else False


    def get_column(self, point):
        """
        Determines which column a given point falls within
        @param point: Point -> Location to determine the column of
        @return: Int -> Column in which point lies
        """
        
        x = point.getX()
        for i in range(7):
            if self.col_barriers[i] <= x <= self.col_barriers[i + 1]:
                return i


    def get_open(self, column):
        """
        Determines the first spot in the given column that is open to receive a piece
        @param column: Int -> Column to find open spot in
        @return: Int -> Row in which a piece can be placed
        """
        
        for i in range(5, -1, -1):
            if self.board[i][column] == None:
                return i


    def play_round(self, starting_player):
        """
        Plays a round of Connect Four
        @param starting_player: Int -> Player which gets to take first turn
        @return: Int -> Player who won the round
        """
        
        # Resets board to default state
        start_back = draw_rectangle(self.window, Point(0, 0), Point(self.window_width, self.window_height), "Black")
        self.win_back.undraw() if self.win_back != None else None
        start_text = draw_text(self.window, Point(self.window_width // 2, self.window_height // 2), f"{self.player_colors[starting_player]} goes first\n\nClick to start", 36, self.player_colors[starting_player])
        for token in self.tokens_on_board:
            token.undraw()
        self.window.getMouse()
        start_text.undraw()
        start_back.undraw()
        self.tokens_on_board = []

        # initializes player and won and begins play_loop
        player = starting_player
        won = False
        while not won:
            # While no one has won, allow player to place a piece
            click = self.window.getMouse()
            col = self.get_column(click)
            row = self.get_open(col)
            self.board[row][col] = player
            token = Token((self.column_width / 2) - (self.column_width / 14), self.col_middle_list[col], self.row_middle_list[row], self.player_colors[player])
            token.draw(self.window)
            self.tokens_on_board.append(token)

            # Checks if move resulted in player winning. If so, return player, else switch player
            won = self.check_for_winner()
            if won:
                return player
            else:
                player = abs(player - 1)


    def win_seq(self, player):
        """
        Operates the sequence that executes when a given player wins a round of Connect Four
        @param player: Int -> Player who won round
        @return: Bool -> User chose to exit game
        """
        
        # Updates scoreboard
        self.scoreboard.update(player)

        # Shows the win screen with play again and exit buttons
        self.win_back = draw_rectangle(self.window, Point(0, 0), Point(self.window_width, self.window_height), "Black")
        win_text = draw_text(self.window, Point(self.window_width//2, self.window_height//2), f"{self.player_colors[player]} Wins!", 36, self.player_colors[player])
        button_height = self.window_height // 5
        button_width = self.window_width // 5
        play_again_anchor = Point(button_width // 2, button_height // 2)
        exit_anchor = Point(self.window_width - button_width // 2, button_height // 2)
        play_again_button = Button("Play Again", play_again_anchor, button_height, button_width,  "Green")
        exit_button = Button("Exit", exit_anchor, button_height, button_width, "Red")
        
        # Waits for user to decide if they want to play again or quit
        clicked_in = False
        play_again_button.draw(self.window)
        exit_button.draw(self.window)
        while not clicked_in:
            click = self.window.getMouse()
            in_play_again = play_again_button.is_in(click)
            in_exit = exit_button.is_in(click)
            clicked_in = in_play_again or in_exit

        # Undraws win screen
        win_text.undraw()
        play_again_button.undraw()
        exit_button.undraw()
        
        return in_exit


    def game_loop(self):
        """
        Operates the game loop
        """
        
        # Initializes Graphic
        self.build_board()

        # Ask users to pick their colors
        self.player_colors = [ColorPicker("Player One", self.window_width, self.window_height).get_color(), ColorPicker("Player Two",self.window_width, self.window_height).get_color()]
        
        # Initializes scoreboard and draws it to graphic
        self.scoreboard = ScoreBoard(self.player_colors[0], self.player_colors[1])
        self.scoreboard.draw(self.window, Point(self.window_width // 2, self.window_height + 10), 20)
        
        # Randomly picks starting player
        starting_player = random.randrange(2)
        
        # Plays rounds as long as users wish
        while self.window.isOpen():
            
            # Plays round and saves winner
            winner = self.play_round(starting_player)
            
            # Runs win_seq and saves user response
            end = self.win_seq(winner)

            # Closes graphic if users selected exit
            self.window.close() if end else None

            # Resets board and allows player who lost to go first on next round
            self.board = [[None for _ in range(7)] for _ in range(6)]
            starting_player = abs(winner - 1)
        

def main():
    test = ConnectFour()
    test.game_loop()


if __name__ == "__main__":
    main()
